#!/usr/bin/env python3

import os
import time
import systemd.daemon
from datetime import datetime

start_over_count = 0

address1 = "<IP addr>"
address2 = "<IP addr>"

fail1 = 0
fail2 = 0

# Delay systemd service to ensure everything is running
time.sleep(30)
systemd.daemon.notify('READY=1')

while True:
    start_over_count += 1
    # print("start over count " + str(start_over_count))

    if fail1 >= 8 & fail2 >= 8:
        # Shutdown VMs before the host
        os.system("virsh shutdown vm1")
        os.system("virsh shutdown vm2")
        os.system("virsh shutdown vm3")

        # Save information to log file
        time_for_log = "System shutdown time was: {}\n".format(datetime.now())
        count_for_file = "Start over count: {}\n".format(start_over_count)
        
        with open("ping_log.txt", "w") as log_file:
            log_file.write(time_for_log)
            log_file.write(count_for_file)

        # Delay host shutdown with 30 seconds
        print("Host shutdown in 30 seconds")
        time.sleep(30)
        os.system("shutdown -h now")
        # os.system('echo "shutdown here"')
        break

    # Wait one minute for next test
    time.sleep(60)

    ping_comm1 = os.system("ping -c1 " + address1) # > /dev/null 2>&1')
    ping_comm2 = os.system("ping -c1 " + address2) # > /dev/null 2>&1')
    # ping_comm1 = os.system("ping -n 1 " + address1)
    # ping_comm2 = os.system("ping -n 1 " + address2)

    if ping_comm1 != 0:
        fail1 += 1
        # print("fail1 count is: " + str(fail1)) 
    else:
        # print("ping1 is OK")
        fail1 = 0

    if ping_comm2 != 0:
        fail2 += 1
        # print("fail2 count is: " + str(fail2))
    else:
        # print("ping2 is OK")
        fail2 = 0